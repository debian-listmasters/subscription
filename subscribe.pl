#!/usr/bin/perl 

{

    package Mojolicious::Command::processqueue;
    use Mojo::Base 'Mojolicious::Command', -signatures;
    use Mail::Transport::SMTP;
    use Mail::Message;

    has description => 'Process sub/unsub queue';
    has usage       => "Usage: APPLICATION processqueue\n";

    sub run ( $self, @args ) {
        my $app = $self->app;
        my $db  = $app->sqlite->db;
        $app->log->info("Processing queue");
        my $sth = $db->query(
q{select * from queue where processed = 0 group by list;}
        );
        while ( my $row = $sth->hash ) {
            my $listname;
            my $to;
            if ($row->{list} =~ /^other-(.+)/) {
                $listname = $1;
                $to = "${listname}-REQUEST\@other.lists.debian.org";
            } else {
                $listname = $row->{list};
                $to = "${listname}-REQUEST\@lists.debian.org";
            }

            print
"Processing $row->{email} for $row->{list} with action $row->{action}\n";
            my $message = Mail::Message->build( 
                data => $row->{action} . " $listname",
                To   => $to,
                From =>  $row->{email},
                Subject =>  $row->{action} . " $listname",
            );

            my $sender = Mail::Transport::SMTP->new();
            $sender->send($message);
            $app->log->info("Processed $row->{action} $listname for $row->{email}");
            my $retry_count = 0;
            my $max_retries = $app->config->{'max_retries'} //5;
            my $wait_time = 0.5;
            my $success = 0;
            while ($retry_count < $max_retries) {
                eval {
                    $db->update('queue', { processed => 1 }, { id => $row->{id} });
                };
                if ($@) {
                    $retry_count++;
                    my $retries_left = $max_retries - $retry_count;
                    $app->log->warn("Failed to update queue entry $row->{id}, retrying in $wait_time seconds - $retries_left retries left");
                    sleep($wait_time * $retry_count);
                } else {
                    $success = 1;
                    last;
                }
            }
            if (!$success) {
                $app->log->error("Failed to update queue entry $row->{id}");
                die "Failed to update queue entry $row->{id}: $@";
            }
        }
    }
}

{

    package Mojolicious::Command::blacklist;
    use Mojo::Base 'Mojolicious::Command', -signatures;
    use Text::Table;

    has description => 'Manage blacklist';
    has usage =>
"Usage: APPLICATION blacklist (list|addip|addmail|deleteip|deletemail) IP/E-Mail\n";

    sub run ( $self, @args ) {
        my $app = $self->app;
        my $db  = $app->sqlite->db;

        if ( !@args ) {
            print $self->usage;
            return;
        }
        if ( $args[0] eq 'list' ) {
            my $sth = $db->query(q{select * from blacklist;});
            my $tb  = Text::Table->new( "IP", "E-Mail", "Timestamp" );
            while ( my $row = $sth->hash ) {
                my $ip    = $row->{ip}    // 'undef';
                my $email = $row->{email} // 'undef';
                $tb->add( $ip, $email, $row->{time} );
            }
            print $tb;
        }
        elsif ( $args[0] eq 'addip' ) {
            if ( !$args[1] ) {
                print $self->usage;
                return;
            }
            $db->insert( 'blacklist', { ip => $args[1] } );
            print "Added $args[1] to blacklist\n";
        }
        elsif ( $args[0] eq 'addmail' ) {
            if ( !$args[1] ) {
                print $self->usage;
                return;
            }
            $db->insert( 'blacklist', { email => $args[1] } );
            print "Added $args[1] to blacklist\n";
        }
        elsif ( $args[0] eq 'deleteip' ) {
            if ( !$args[1] ) {
                print $self->usage;
                return;
            }
            $db->delete( 'blacklist', { ip => $args[1] } );
            print "Deleted $args[1] from blacklist\n";
        }
        elsif ( $args[0] eq 'deletemail' ) {
            if ( !$args[1] ) {
                print $self->usage;
                return;
            }
            $db->delete( 'blacklist', { ip => $args[1] } );
            print "Deleted $args[1] from blacklist\n";
        }
        else {
            print $self->usage;
        }
    }

}

use Mojolicious::Lite -signatures;
use Mojo::SQLite;
use Data::Dumper;
use Mojo::Log;

use Data::Validate::Email qw(is_email);

my $config = plugin Config => { file => '../subscribe/subscribe.conf' };

if ( -e '../subscribe/secret' ) {
    open my $fh, '<', '../subscribe/secret' or die "Can't open secret file: $!";
    my $secret
        = do { local $/; <$fh> };
    close $fh;
    app->secrets($secret);
}

 app->log( Mojo::Log->new( path => $config->{'log_file'},
    level => $config->{'log_level'} ) );

helper sqlite => sub {
    state $sql = Mojo::SQLite->new('sqlite:../subscribe/subscribe.db');
    my $migrations = $sql->migrations->from_data;
    $migrations = $migrations->migrate;

    return $sql;
};

get '/' => sub ($c) {
    $c->render( text => 'There is nothing to see here.' );
};

post '/' => sub ($c) {

    my $retry_count = 0;
    my $max_retries = $c->config->{'max_retries'} // 5;
    my $wait_time = 0.5;
    my $success = 0;
    while ($retry_count < $max_retries) {
        eval {
            $c->sqlite->db->insert( 'connections', { ip => $c->tx->remote_address } );
        };
        if ($@) {
            $retry_count++;
            my $retries_left = $max_retries - $retry_count;
            $c->log->warn("Failed to insert connection entry, retrying in $wait_time seconds - $retries_left retries left");
            sleep($wait_time * $retry_count);
        } else {
            $success = 1;
            last;
        }
    }
    if (!$success) {
        $c->log->error("Failed to insert connection entry");
        $c->render(
            template => 'response',
            title    => 'Error',
            content  => 'Failed to insert connection entry. Please try again later.'
        );
        return;
    }
    $c->app->log->debug("Connection from " . $c->tx->remote_address);

    my $db  = $c->sqlite->db;
    my $cnt = $db->query(
q{select count(*) as cnt from connections where ip = ? and time > datetime('now', ?); },
        $c->tx->remote_address, $config->{'time_frame'}
    )->hash->{cnt};

    if ( $cnt >= $config->{'max_connections'} ) {
        $c->app->log->warn("Too many connections from " . $c->tx->remote_address);
        $c->render(
            template => 'response',
            title    => 'Too many connections',
            content  =>
'Too many connections from your IP address. <br>Please come back later.'
        );
        return;
    }

    $cnt = $db->query( q{select count(*) as cnt from blacklist where ip = ?; },
        $c->tx->remote_address )->hash->{cnt};

    if ( $cnt >= 1 ) {
        $c->app->log->warn("Blocked IP " . $c->tx->remote_address);
        $c->render(
            template => 'response',
            title    => 'Blocked',
            content  =>
'Your ip is blocked. Please contact listmaster if you think this is a mistake.'
        );
        return;
    }

    if ( $c->param('user_email') ) {
        $c->app->log->debug("Email: " . $c->param('user_email'));
        if ( !is_email( $c->param('user_email') ) ) {
            $c->app->log->warn("Invalid email: " . $c->param('user_email'));
            $c->render(
                template => 'response',
                title    => 'Invalid email',
                content  => 'Invalid email address.'
            );
            return;
        }
    }
    else {
        $c->app->log->debug("No email provided");
        $c->render(
            template => 'response',
            title    => 'No email',
            content  => 'No email address provided.'
        );
        return;
    }

    my $action;
    if ($c->param('subscribe')) {
        $action = 'subscribe';
    } elsif ($c->param('unsubscribe')) {
        $action = 'unsubscribe';
    } elsif ($c->param('action')) {
        $action = lc( $c->param('action') );
    } else {
        $c->app->log->debug("No action provided");
        $c->app->log->debug(Dumper($c->req->params->to_hash));
        $c->render(
            template => 'response',
            title    => 'No action',
            content  => 'No action provided.'
        );
        return;
    }

    if ($action !~ /^(subscribe|unsubscribe)$/) {
        $c->app->log->warn("Invalid action: $action");
        $c->render(
            template => 'response',
            title    => 'Invalid action',
            content  => 'Invalid action.'
        );
        return;
    }
    my $subscribe_count = scalar @{$c->every_param('subscribe')};
    my $max_subscriptions =  $c->config->{'max_subscriptions'} //= 5;

    if ($subscribe_count > $max_subscriptions) {
        $c->app->log->warn("Too many subscriptions: $subscribe_count");
        $c->render(
            template => 'response',
            title    => 'Too many subscriptions',
            content  => "You can only work on a maximum of $max_subscriptions lists."
        );
        return;
    }
    
    if ( $action ) {
        foreach my $list ( @{ $c->every_param('subscribe') }, @{ $c->every_param('list') }   ) {
            $c->app->log->info("$action " . $c->param('user_email')  . "to $list");
            if ( $list =~ /^[a-z0-9-]+/ ) {
                my $retry_count = 0;
                my $max_retries = $c->config->{'max_retries'} // 5;
                my $wait_time = 0.5;
                my $success = 0;
                while ($retry_count < $max_retries) {
                    eval {
                        $db->insert(
                            'queue',
                            {
                                email  => $c->param('user_email'),
                                action => $action,
                                list   => $list
                            }
                        );
                    };
                    if ($@) {
                        $retry_count++;
                        my $retries_left = $max_retries - $retry_count;
                        $c->log->warn("Failed to insert action entry, retrying in $wait_time seconds - $retries_left retries left");
                        sleep($wait_time * $retry_count);
                    } else {
                        $success = 1;
                        last;
                    }
                }
                if (!$success) {
                    $c->log->error("Failed to insert action entry");
                    $c->render(
                        template => 'response',
                        title    => 'Error',
                        content  => 'Failed to process your request. Please try again later.'
                    );
                    return;
                }

            }
            else {
                $c->app->log->warn("Invalid list: $list");
                $c->render(
                    template => 'response',
                    title    => 'Invalid list',
                    content  => 'Invalid list name.'
                );
                return;
            }
        }
        $c->render(
            template => 'response',
            title    => $action,
            content  =>
"You have been ${action}d to the list(s). <br /> I can take up to 5 minutes until we process your request. <br />You will have to confirm your request."
        );
        return;
    }

};
app->start;

__DATA__
@@ response.html.ep
<!DOCTYPE html>
<html>
  <head><title> <%= $title %> </title></head>
  <body> <%== $content %> </body>
</html>

@@ migrations

-- 1 up
CREATE TABLE connections (
  id INTEGER PRIMARY KEY,
  ip TEXT NOT NULL,
  time DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE blacklist (
  id INTEGER PRIMARY KEY,
  ip TEXT,
  email TEXT,
  time DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
);
-- 1 down

DROP TABLE IF EXISTS connections;
DROP TABLE IF EXISTS blacklist;

-- 2 up

CREATE TABLE queue (
  id INTEGER PRIMARY KEY,
  email TEXT NOT NULL,
  action TEXT NOT NULL,
  list TEXT NOT NULL,
  time DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  processed BOOLEAN NOT NULL DEFAULT FALSE
);

-- 2 down

DROP TABLE IF EXISTS queue;
