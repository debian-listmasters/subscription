# subscribe.pl

## Description

This repository contains the code for the `subscribe.pl` script, which allows users to subscribe or unsubscribe to a Debian Mailinglists.
It is installed in $HOME/subscribe of the listweb user and is symlinked to cgi-bin. There is a logfile in the home of the application. 

Subscribe is a Mojolicous Perl Applikation that processes requests from https://www.debian.org/MailingLists/subscribe and https://www.debian.org/MailingLists/unsubscribe. Every request is written to the sqlite database and gets processed via cron every 5 minutes. 

## Blacklisting

You can block ips or emails from subscribing (no patterns so far). By using the blacklist helper:

```bash
./subscribe.pl blacklist (list|addip|addmail|deleteip|deletemail) IP/E-Mail
```

## Processing the queue

If you want to you can process the queue by hand via the processqueue helper 

```bash 
./subscribe.pl processqueue
```

## Configfile 

there is a configfile subscribe.conf which is probably self explaining. 


